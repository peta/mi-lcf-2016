#pragma once

#include <map>
#include <deque>
#include "llvm.h"
#include "llvm_runtime.h"

namespace rift {

/** Our abstract value is pointer to a llvm::Value.
 */
typedef llvm::Value * AValue;

/** Abstract state assigns each variable a llvm::Value in which it may already be contained.
 */
class AState {
public:

    /** Merges the other abstract state into itself. Returns true if any change happened, false otherwise.
     */
    bool merge(AState const & other) {
        bool result = false;
        // ignore all variables in other that we do not have (they must be set to top)
        // if both have, keep only if identical, otherwise set to top (erase)
        for (auto & i : other.state_) {
            auto j = state_.find(i.first);
            if (j != state_.end()) {
                if (j->second != i.second) {
                    state_.erase(j);
                    result = true;
                }
            }
        }
        // any variables which we have and the other does not have must be deleted (set to top
        auto i = state_.begin();
        while (i != state_.end()) {
            auto j = other.state_.find(i->first);
            if (j == other.state_.end()) {
                auto ii = i;
                ++i;
                state_.erase(ii);
            } else {
                ++i;
            }
        }
        return result;
    }

    /** Sets register for given symbol, meaning the symbol can be found in the specified register.
     */
    void set(unsigned symbol, AValue value) {
        state_[symbol] = value;
    }

    /** Returns register that contains given symbol, or nullptr if no such register exists.
     */
    AValue get(unsigned symbol) {
        auto i = state_.find(symbol);
        if (i == state_.end())
            return nullptr;
        else
            return i->second;
    }

    /** Clears all information about variables being stored in registers.
     */
    void setAllToTop() {
        state_.clear();
    }

private:

    std::map<unsigned, AValue> state_;

};

namespace analysis {

    class LoadElimination : public llvm::FunctionPass {
    public:
        static char ID;

        LoadElimination():
            llvm::FunctionPass(ID) {
        }

        char const * getPassName() const override {
            return "LoadEliminationAnalysis";
        }

        /** In the optimizer, returns a register that can be used as a replacemend for envGet call, or nullptr if the read cannot be removed.
         */
        llvm::Value * replaceLoad(llvm::Value * what) {
            auto i = storedLoads_.find(what);
            if (i == storedLoads_.end())
                return nullptr;
            else
                return i->second;
        }

        bool runOnFunction(llvm::Function & f) {
            incoming_.clear();
            storedLoads_.clear();
            RiftModule *m = reinterpret_cast<RiftModule*>(f.getParent());
            // push first basic block to the queue
            q_.push_back(&* f.begin());
            while (not q_.empty()) {
                // for each basic block in the queue take it out and analyze
                llvm::BasicBlock * b = q_.front();
                q_.pop_front();
                // get its incoming state
                AState state = incoming_[b];
                // update the state with all instructions in the state
                for (auto i = b->begin(), e = b->end(); i != e; ++i) {
                    llvm::Instruction * ins = &* i;
                    if (llvm::CallInst * ci = llvm::dyn_cast<llvm::CallInst>(ins)) {
                        llvm::Function * cf = ci->getCalledFunction();
                        if (cf == m->genericEval or cf == m->call) {
                            state.setAllToTop();
                        } else if (cf == m->envSet) {
                            unsigned symbol = fromLlvmValue(ci->getArgOperand(1));
                            llvm::Value * reg = ci->getArgOperand(2);
                            state.set(symbol, reg);
                        } else if (cf == m->envGet) {
                            unsigned symbol = fromLlvmValue(ci->getArgOperand(1));
                            AValue reg = state.get(symbol);
                            if (reg == nullptr) {
                                state.set(symbol, cf);
                                storedLoads_[ci] = nullptr;
                            } else {
                                storedLoads_[ci] = reg;
                            }
                        }
                    } else if (llvm::BranchInst * bi = llvm::dyn_cast<llvm::BranchInst>(ins)) {
                        unsigned x = bi->getNumSuccessors();
                        for (unsigned j = 0; j < x; ++j) {
                            llvm::BasicBlock * target = bi->getSuccessor(j);
                            // if we get there first time, store the state
                            if (incoming_.find(target) == incoming_.end()) {
                                incoming_[target] = state;
                                q_.push_back(target);
                            } else {
                                if (incoming_[target].merge(state))
                                    q_.push_back(target);
                            }
                        }
                    }
                }
            }
            incoming_.clear();
            printResults();
            return false;
        }

        void printResults() {
            for (auto i : storedLoads_) {
                if (i.second != nullptr) {
                    std::cout << "load " << std::endl;
                    i.first->dump();
                    std::cout << "can be replaced with " << std::endl;
                    i.second->dump();
                }
            }
        }

    private:
        std::deque<llvm::BasicBlock *> q_;
        std::map<llvm::BasicBlock *, AState> incoming_;
        /** This is going to be the analysis result. For each load instruction that loads value already stored in a register, the map contains the register.
         */
        std::map<llvm::Value *, llvm::Value *> storedLoads_;
    };
}


namespace optimization {

    class LoadElimination : public llvm::FunctionPass {
    public:
        static char ID;

        LoadElimination():
            llvm::FunctionPass(ID) {
        }

        char const * getPassName() const override {
            return "LoadEliminationOptimization";
        }

        void getAnalysisUsage(llvm::AnalysisUsage & AU) const override {
            AU.addRequired<analysis::LoadElimination>();
            AU.addPreserved<analysis::LoadElimination>();
        }


        bool runOnFunction(llvm::Function & f) override {
            analysis::LoadElimination & a = getAnalysis<analysis::LoadElimination>();
            RiftModule *m = reinterpret_cast<RiftModule*>(f.getParent());
            bool result = false;
            // for each basic block
            for (auto & b : f) {
                auto i = b.begin();
                while (i != b.end()) {
                    // get the instruction
                    llvm::Instruction * ins = &* i;
                    // move the iterator
                    ++i;
                    // if the instruction is not a terminator
                    if (llvm::CallInst * ci = llvm::dyn_cast<llvm::CallInst>(ins)) {
                        llvm::Function * cf = ci->getCalledFunction();
                        if (cf == m->envGet) {
                            llvm::Value * replacement = a.replaceLoad(ins);
                            if (replacement != nullptr) {
                                ci->replaceAllUsesWith(replacement);
                                result = true;
                            }
                        }
                    }
                }
            }
            return result;
        }
    };
}



}

