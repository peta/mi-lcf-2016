#pragma once

#include <ciso646>

#include "ast.h"
#include "runtime.h"

namespace rift {

/** A very simple interpreter for Rift.
 */
class Interpreter : public Visitor {
public:

    static RVal * interpret(ast::Fun * ast, Environment * env) {
        Interpreter i(env);
        ast->body->accept(&i);
        return i.result_;
    }

    void visit(ast::Exp * node) override {
        throw "You are missing some implementation of expression subtype";
    }

    void visit(ast::Num * node) override {
        result_ = fromDoubleVector(doubleVectorLiteral(node->value));
    }

    void visit(ast::Str * node) override {
        result_ = fromCharacterVector(characterVectorLiteral(node->index));
    }

    void visit(ast::Var * node) override {
        result_ = envGet(env_, node->symbol);
    }

    void visit(ast::Seq * node) override {
        for (ast::Exp * e : node->body)
            e->accept(this);
    }

    /** Creates new function
     */
    void visit(ast::Fun * node) override {
        result_ = fromFunction(new Function(node, env_));
    }

    void visit(ast::BinExp * node) override {
        node->lhs->accept(this);
        RVal * lhs = result_;
        node->rhs->accept(this);
        RVal * rhs = result_;
        switch (node->type) {
        case ast::BinExp::Type::add:
            result_ = genericAdd(lhs, rhs);
            break;
        case ast::BinExp::Type::sub:
            result_ = genericSub(lhs, rhs);
            break;
        case ast::BinExp::Type::mul:
            result_ = genericMul(lhs, rhs);
            break;
        case ast::BinExp::Type::div:
            result_ = genericDiv(lhs, rhs);
            break;
        case ast::BinExp::Type::eq:
            result_ = genericEq(lhs, rhs);
            break;
        case ast::BinExp::Type::neq:
            result_ = genericNeq(lhs, rhs);
            break;
        case ast::BinExp::Type::lt:
            result_ = genericLt(lhs, rhs);
            break;
        case ast::BinExp::Type::gt:
            result_ = genericGt(lhs, rhs);
            break;
        }
    }

    void visit(ast::UserCall * node) override {
        node->name->accept(this);
        RVal * f = result_;
        std::vector<RVal*> args;
        for (auto arg : node->args) {
            arg->accept(this);
            args.push_back(result_);
        }
        result_ = call(f, args);
    }

    void visit(ast::CCall * node) override {
        std::vector<RVal*> args;
        for (auto arg : node->args) {
            arg->accept(this);
            args.push_back(result_);
        }
        result_ = concat(args);
    }

    void visit(ast::EvalCall * node) override {
        node->args[0]->accept(this);
        result_ = genericEval(env_, result_);
    }

    void visit(ast::TypeCall * node) override {
        node->args[0]->accept(this);
        result_ = fromCharacterVector(type(result_));
    }

    void visit(ast::LengthCall * node) override {
        node->args[0]->accept(this);
        result_ = fromDoubleVector(doubleVectorLiteral(length(result_)));
    }

    void visit(ast::Index * node) override {
        node->name->accept(this);
        RVal * object = result_;
        node->index->accept(this);
        RVal * index = result_;
        result_ = genericGetElement(object, index);
    }

    void visit(ast::Assignment * node) override {
    }

    void visit(ast::SimpleAssignment * node) override {
        node->rhs->accept(this);
        envSet(env_, node->name->symbol, result_);
    }

    void visit(ast::IndexAssignment * node) override {
        node->rhs->accept(this);
        RVal * rhs = result_;
        node->index->name->accept(this);
        RVal * object = result_;
        node->index->index->accept(this);
        RVal * index = result_;
        genericSetElement(object,  index, rhs);
        result_ = rhs;

    }

    void visit(ast::IfElse * node) override {
        node->guard->accept(this);
        bool cond = toBoolean(result_);
        if (cond)
            node->ifClause->accept(this);
        else
            node->elseClause->accept(this);
    }

    void visit(ast::WhileLoop * node) override {
        while (true) {
            node->guard->accept(this);
            if (not toBoolean(result_))
                break;
            node->body->accept(this);
        }
    }

private:

    Interpreter(Environment * env):
        env_(env),
        result_(nullptr) {
    }

    Environment * env_;
    RVal * result_;
};

}

