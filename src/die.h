#pragma once

#include "llvm.h"
#include "llvm_runtime.h"

namespace rift {

class DIE : public llvm::FunctionPass {
public:
    static char ID;

    DIE() : llvm::FunctionPass(ID) {
    }

    char const * getPassName() const override {
        return "DeadInstructionElimination";
    }

    /** HOMEWORK: This does not really work, because we delete instructions regardless of their sideeffects. Fix it, please:)
     */
    bool runOnFunction(llvm::Function & f) override {
        bool result = false;
        // for each basic block
        for (auto & b : f) {
            auto i = b.begin();
            while (i != b.end()) {
                // get the instruction
                llvm::Instruction * ins = &* i;
                // move the iterator
                ++i;
                // if the instruction is not a terminator
                if (not ins->isTerminator())
                    // and if it has no uses
                    if (ins->user_begin() == ins->user_end()) {
                        ins->eraseFromParent();
                        result = true;
                    }
            }
        }
        return result;
    }
};
}
