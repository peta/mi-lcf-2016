#pragma once

#include <ciso646>
#include "llvm.h"
#include "llvm_types.h"
#include "llvm_runtime.h"
#include "ast.h"
#include "runtime.h"
#include "optimizer.h"

namespace rift {

class Compiler : public Visitor {
public:
    /** Compiles the given function and returns a pointer to the compiled function.
     */
    static FunPtr compile(ast::Fun * fun) {
        Compiler c;
        // no need to worry about arguments, compile the body of the function
        fun->body->accept(&c);
        // we must create the return instruction now
        llvm::ReturnInst::Create(llvm::getGlobalContext(), c.result_, c.b_);
        // now that we have finalized the compilation of the module to the IR, we can finalize it and get the pointer to the compiled code
        llvm::ExecutionEngine * engine = llvm::EngineBuilder(std::unique_ptr<llvm::Module>(c.m_)).setMCJITMemoryManager(std::unique_ptr<RiftMemoryManager>(new RiftMemoryManager())).create();
        Optimizer::optimize(c.m_, engine);
        engine->finalizeObject();
        return reinterpret_cast<FunPtr>(engine->getPointerToFunction(c.f_));
    }

    /** Shorthand for calling runtime functions.  */
#define RUNTIME_CALL(name, ...) \
    llvm::CallInst::Create(m_->name, \
            std::vector<llvm::Value*>({__VA_ARGS__}), \
            "", \
            b_)

    llvm::Value * fromDouble(double value) {
        return llvm::ConstantFP::get(llvm::getGlobalContext(), llvm::APFloat(value));
    }

    llvm::Value * fromInt(int value) {
        return llvm::ConstantInt::get(llvm::getGlobalContext(), llvm::APInt(32, value));
    }


    void visit(ast::Num * node) override {
        result_ = fromDouble(node->value);
        result_ = RUNTIME_CALL(doubleVectorLiteral, result_);
        result_ = RUNTIME_CALL(fromDoubleVector, result_);
    }

    void visit(ast::Str * node) override {
        throw "NOT IMPLEMENTED";
    }

    void visit(ast::Var * node) override {
        throw "NOT IMPLEMENTED";
    }

    void visit(ast::Seq * node) override {
        for (ast::Exp * e : node->body)
            e->accept(this);
    }

    void visit(ast::Fun * node) override {
        throw "NOT IMPLEMENTED";
    }

    void visit(ast::BinExp * node) override {
        throw "NOT IMPLEMENTED";
    }

    void visit(ast::UserCall * node) override {
        throw "NOT IMPLEMENTED";
    }

    void visit(ast::CCall * node) override {
        throw "NOT IMPLEMENTED";
    }

    void visit(ast::EvalCall * node) override {
        throw "NOT IMPLEMENTED";
    }

    void visit(ast::TypeCall * node) override {
        throw "NOT IMPLEMENTED";
    }

    void visit(ast::LengthCall * node) override {
        throw "NOT IMPLEMENTED";
    }

    void visit(ast::Index * node) override {
        throw "NOT IMPLEMENTED";
    }

    void visit(ast::SimpleAssignment * node) override {
        throw "NOT IMPLEMENTED";
    }

    void visit(ast::IndexAssignment * node) override {
        throw "NOT IMPLEMENTED";
    }
#ifdef HAHA
    node->guard->accept(this);
    bool cond = toBoolean(result_);
    if (cond)
        node->ifClause->accept(this);
    else
        node->elseClause->accept(this);
#endif


    void visit(ast::IfElse * node) override {
        node->guard->accept(this);
        result_ = RUNTIME_CALL(toBoolean, result_);

        llvm::BasicBlock * bTrue = llvm::BasicBlock::Create(llvm::getGlobalContext(), "trueCase", f_, nullptr);
        llvm::BasicBlock * bFalse = llvm::BasicBlock::Create(llvm::getGlobalContext(), "falseCase", f_, nullptr);
        llvm::BasicBlock * bEnd = llvm::BasicBlock::Create(llvm::getGlobalContext(), "ifEnd", f_, nullptr);

        llvm::BranchInst::Create(bTrue, bFalse, result_, b_);

        b_ = bTrue;
        node->ifClause->accept(this);
        llvm::BranchInst::Create(bEnd,b_);
        llvm::Value * rTrue = result_;
        bTrue = b_;

        b_ = bFalse;
        node->elseClause->accept(this);
        llvm::BranchInst::Create(bEnd,b_);
        llvm::Value * rFalse = result_;
        bFalse = b_;

        b_ = bEnd;
        llvm::PHINode * phi = llvm::PHINode::Create(t::ptrValue, 2, "ifPhi", b_);
        phi->addIncoming(rTrue, bTrue);
        phi->addIncoming(rFalse, bFalse);

        result_ = phi;


    }

    void visit(ast::WhileLoop * node) override {
        throw "NOT IMPLEMENTED";
    }

private:
    Compiler():
        m_(new RiftModule()),
        f_(llvm::Function::Create(t::NativeCode, llvm::Function::ExternalLinkage, "riftFunction", m_)),
        b_(llvm::BasicBlock::Create(llvm::getGlobalContext(), "entry", f_, nullptr))
    {
        // get argument's value, which is the environment of the function
        llvm::Function::arg_iterator args = f_->arg_begin();
        env_ = & (*args);
        env_->setName("env");
        // we are now ready to compile the rest
    }




    RiftModule * m_;
    llvm::Function * f_;
    llvm::BasicBlock * b_;
    llvm::Value * env_;
    llvm::Value * result_;

};

}
