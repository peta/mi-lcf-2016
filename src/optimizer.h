#pragma once

#include "llvm.h"
#include "die.h"
#include "loade.h"

namespace rift {

class Optimizer {
public:
    static void optimize(llvm::Module * m, llvm::ExecutionEngine * ee) {
        auto * pm = new llvm::legacy::FunctionPassManager(m);
        m->setDataLayout(ee->getDataLayout());
        pm->add(new analysis::LoadElimination());
        pm->add(new optimization::LoadElimination());
        pm->add(new DIE());
        for (llvm::Function & f : *m) {
            if (f.isDeclaration())
                continue;
            std::cout << " ------------ Before optimization " << std::endl;
            unsigned i = 0;
            f.dump();
            while (pm->run(f)) {
                std::cout << " ------------ After pass " << ++i << std::endl;
                f.dump();
            }
        }
        delete pm;
    }
};

}


