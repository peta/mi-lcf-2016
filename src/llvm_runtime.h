#pragma once

#include "llvm.h"
#include "llvm_types.h"
#include "runtime.h"

/** Given llvm::Value that is an unsigned constant, returns the unsigned value.
 */
inline unsigned fromLlvmValue(llvm::Value* val) {
    if (llvm::ConstantInt* cint = llvm::dyn_cast<llvm::ConstantInt>(val)) {
      if (cint->getBitWidth() <= 32) {
        return cint->getZExtValue();
      }
    }
    assert(false);
    return 0;
}

/** Rift module inherits from LLVM module and replaces it in the compiler.

  Apart from LLVM module's usage it also contains declarations of the runtime functions the module might use.
  */
class RiftModule : public llvm::Module {

public:
    RiftModule():
        llvm::Module("rift", llvm::getGlobalContext()) {
    }

    /** Shorthand macro for declaring functions that are not pure.
    */
#define DEF_FUN(name, signature) llvm::Function * name = llvm::Function::Create(signature, llvm::Function::ExternalLinkage, #name, this)

    /** All runtime functions must be declared properly. Declaration consists of the name of the function (i.e. the symbol under which it can be found) and the type.
    */
    DEF_FUN(doubleVectorLiteral, t::dv_d);
    DEF_FUN(characterVectorLiteral, t::cv_i);
    DEF_FUN(fromDoubleVector, t::v_dv);
    DEF_FUN(fromCharacterVector, t::v_cv);
    DEF_FUN(fromFunction, t::v_f);
    DEF_FUN(genericGetElement, t::v_vv);
    DEF_FUN(genericSetElement, t::void_vvv);
    DEF_FUN(envGet, t::v_ei);
    DEF_FUN(envSet, t::void_eiv);
    DEF_FUN(genericAdd, t::v_vv);
    DEF_FUN(genericSub, t::v_vv);
    DEF_FUN(genericMul, t::v_vv);
    DEF_FUN(genericDiv, t::v_vv);
    DEF_FUN(genericEq, t::v_vv);
    DEF_FUN(genericNeq, t::v_vv);
    DEF_FUN(genericLt, t::v_vv);
    DEF_FUN(genericGt, t::v_vv);
    DEF_FUN(createFunction, t::f_ie);
    DEF_FUN(toBoolean, t::b_v);
    DEF_FUN(call, t::v_viVA);
    DEF_FUN(length, t::d_v);
    DEF_FUN(type, t::cv_v);
    DEF_FUN(genericEval, t::v_ev);
    DEF_FUN(c, t::v_iVA);
};

/** The Rift Memory manager extends the default LLVM memory manager with
    support for resolving the Rift runtime functions. This is achieved by
    extending the behavior of the getSymbolAddress function.
  */
class RiftMemoryManager : public llvm::SectionMemoryManager {

public:
#define NAME_IS(name) if (Name == #name) return reinterpret_cast<uint64_t>(::name)
    /** Return the address of symbol, or nullptr if undefind. We extend the
        default LLVM resolution with the list of RIFT runtime functions.
      */
    uint64_t getSymbolAddress(const std::string & Name) override {
        uint64_t addr = SectionMemoryManager::getSymbolAddress(Name);
        if (addr != 0) return addr;
        // This bit is for some OSes (Windows and OSX where the MCJIT symbol
        // loading is broken)
        NAME_IS(envCreate);
        NAME_IS(envGet);
        NAME_IS(envSet);
        NAME_IS(doubleVectorLiteral);
        NAME_IS(characterVectorLiteral);
        NAME_IS(fromDoubleVector);
        NAME_IS(fromCharacterVector);
        NAME_IS(fromFunction);
        NAME_IS(genericGetElement);
        NAME_IS(genericSetElement);
        NAME_IS(genericAdd);
        NAME_IS(genericSub);
        NAME_IS(genericMul);
        NAME_IS(genericDiv);
        NAME_IS(genericEq);
        NAME_IS(genericNeq);
        NAME_IS(genericLt);
        NAME_IS(genericGt);
//        NAME_IS(createFunction);
        NAME_IS(toBoolean);
        NAME_IS(call);
        NAME_IS(length);
        NAME_IS(type);
        NAME_IS(eval);
        NAME_IS(genericEval);
        NAME_IS(c);
        llvm::report_fatal_error("Extern function '" + Name + "' couldn't be resolved!");
    }
};

