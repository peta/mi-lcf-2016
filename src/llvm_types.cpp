#include "llvm_types.h"

namespace t {

/** Initialization type declarations. Each Rift type must be declared to
    LLVM.
  */
#define STRUCT(name, ...) \
    llvm::StructType::create(name, __VA_ARGS__, nullptr)
#define FUN_TYPE(result, ...) \
    llvm::FunctionType::get(result, std::vector<llvm::Type*>({ __VA_ARGS__}), false)
#define FUN_TYPE_VARARG(result, ...) \
    llvm::FunctionType::get(result, std::vector<llvm::Type*>({ __VA_ARGS__}), true)

llvm::StructType * environmentType();

llvm::Type * Void = llvm::Type::getVoidTy(llvm::getGlobalContext());
llvm::Type * Int = llvm::IntegerType::get(llvm::getGlobalContext(), 32);
llvm::Type * Double = llvm::Type::getDoubleTy(llvm::getGlobalContext());
llvm::Type * Character = llvm::IntegerType::get(llvm::getGlobalContext(), 8);
llvm::Type * Bool = llvm::IntegerType::get(llvm::getGlobalContext(), 1);

llvm::PointerType * ptrInt = llvm::PointerType::get(Int, 0);
llvm::PointerType * ptrCharacter = llvm::PointerType::get(Character, 0);
llvm::PointerType * ptrDouble = llvm::PointerType::get(Double, 0);

llvm::StructType * DoubleVector = STRUCT("DoubleVector", ptrDouble, Int);
llvm::StructType * CharacterVector = STRUCT("CharacterVector", ptrCharacter, Int);
llvm::PointerType * ptrDoubleVector = llvm::PointerType::get(DoubleVector, 0);
llvm::PointerType * ptrCharacterVector = llvm::PointerType::get(CharacterVector, 0);

llvm::StructType * Value = STRUCT("Value", Int, ptrDoubleVector);
llvm::PointerType * ptrValue = llvm::PointerType::get(Value, 0);

llvm::StructType * Binding = STRUCT("Binding", Int, ptrValue);
llvm::PointerType * ptrBinding = llvm::PointerType::get(Binding, 0);

llvm::PointerType * ptrEnvironment;
llvm::StructType * Environment = environmentType();

llvm::FunctionType * NativeCode = FUN_TYPE(ptrValue, ptrEnvironment);

llvm::StructType * Function = STRUCT("Function", ptrEnvironment, NativeCode, ptrInt, Int);
llvm::PointerType * ptrFunction = llvm::PointerType::get(Function, 0);

llvm::FunctionType * dv_d = FUN_TYPE(ptrDoubleVector, Double);
llvm::FunctionType * cv_i = FUN_TYPE(ptrCharacterVector, Int);
llvm::FunctionType * v_dv = FUN_TYPE(ptrValue, ptrDoubleVector);
llvm::FunctionType * v_cv = FUN_TYPE(ptrValue, ptrCharacterVector);
llvm::FunctionType * v_ev = FUN_TYPE(ptrValue, ptrEnvironment, ptrValue);
llvm::FunctionType * v_vv = FUN_TYPE(ptrValue, ptrValue, ptrValue);
llvm::FunctionType * v_vvv = FUN_TYPE(ptrValue, ptrValue, ptrValue, ptrValue);
llvm::FunctionType * v_vi = FUN_TYPE(ptrValue, ptrValue, Int);
llvm::FunctionType * v_viv = FUN_TYPE(ptrValue, ptrValue, Int, ptrValue);
llvm::FunctionType * v_ei = FUN_TYPE(ptrValue, ptrEnvironment, Int);
llvm::FunctionType * void_eiv = FUN_TYPE(Void, ptrEnvironment, Int, ptrValue);
llvm::FunctionType * dv_dvdv = FUN_TYPE(ptrDoubleVector, ptrDoubleVector, ptrDoubleVector);
llvm::FunctionType * cv_cvcv = FUN_TYPE(ptrCharacterVector, ptrCharacterVector, ptrCharacterVector);
llvm::FunctionType * dv_cvcv = FUN_TYPE(ptrDoubleVector, ptrCharacterVector, ptrCharacterVector);
llvm::FunctionType * d_dvd = FUN_TYPE(Double, ptrDoubleVector, Double);
llvm::FunctionType * cv_cvdv = FUN_TYPE(ptrCharacterVector, ptrCharacterVector, ptrDoubleVector);

llvm::FunctionType * v_f = FUN_TYPE(ptrValue, ptrFunction);
llvm::FunctionType * f_ie = FUN_TYPE(ptrFunction, Int, ptrEnvironment);

llvm::FunctionType * b_v = FUN_TYPE(Bool, ptrValue);

llvm::FunctionType * v_viVA = FUN_TYPE_VARARG(ptrValue, ptrValue, Int);

llvm::FunctionType * void_vvv = FUN_TYPE(Void, ptrValue, ptrValue, ptrValue);
llvm::FunctionType * void_dvdvdv = FUN_TYPE(Void, ptrDoubleVector, ptrDoubleVector, ptrDoubleVector);
llvm::FunctionType * void_cvdvcv = FUN_TYPE(Void, ptrCharacterVector, ptrDoubleVector, ptrCharacterVector);
llvm::FunctionType * void_dvdd = FUN_TYPE(Void, ptrDoubleVector, Double, Double);

llvm::FunctionType * d_v = FUN_TYPE(Double, ptrValue);
llvm::FunctionType * cv_v = FUN_TYPE(ptrCharacterVector, ptrValue);
llvm::FunctionType * v_iVA = FUN_TYPE_VARARG(ptrValue, Int);
llvm::FunctionType * dv_iVA = FUN_TYPE_VARARG(ptrDoubleVector, Int);
llvm::FunctionType * cv_iVA = FUN_TYPE_VARARG(ptrCharacterVector, Int);

llvm::FunctionType * dv_v = FUN_TYPE(ptrDoubleVector, ptrValue);
llvm::FunctionType * d_dv = FUN_TYPE(Double, ptrDoubleVector);
llvm::FunctionType * f_v = FUN_TYPE(ptrFunction, ptrValue);

llvm::StructType * environmentType() {
    llvm::StructType * result = llvm::StructType::create(llvm::getGlobalContext(), "Environment");
    ptrEnvironment = llvm::PointerType::get(result, 0);
    result->setBody(ptrEnvironment, ptrBinding, Int, nullptr);
    return result;
}
}
