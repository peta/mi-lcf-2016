# rift

This is the example project for MI-LCF 2016. 

## How to build:

> You need to have llvm, cmake and g++ installed.

```
mkdir build
cd build
cmake ..
make
./rift
```

## Version 1

The first iteration of *rift* does not do much. Syntax errors will be reported and REPL will print the parsed AST for each expression you type.


